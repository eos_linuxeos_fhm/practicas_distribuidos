/* tcpclient.c */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

int main(void)
{
  int sock, bytes_recieved;  
  char send_data[1024],recv_data[1024],cadena[]="date --set \"20";
  struct hostent *host;
  struct sockaddr_in server_addr;  

  host = gethostbyname("127.0.0.1");

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("Socket");
      exit(1);
  }

  server_addr.sin_family = AF_INET;     
  server_addr.sin_port = htons(5000);   
  server_addr.sin_addr = *((struct in_addr *)host->h_addr);
  bzero(&(server_addr.sin_zero),8); 

  if (connect(sock, (struct sockaddr *)&server_addr,
              sizeof(struct sockaddr)) == -1) 
  {
      perror("Connect");
      exit(1);
  }
  send(sock,"dame la hora",strlen("dame la hora"), 0);
  bytes_recieved=recv(sock,recv_data,1024,0);
  recv_data[bytes_recieved] = '\0';
  //printf("%s\n",recv_data);
  close(sock);

  strcat(cadena,recv_data);
  strcat(cadena,"\"");
  //puts(cadena);
  //system("date --set \"2007-05-27 17:27\"");
  system(cadena);
  return 0;
}
