/* tcpserver.c */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>

 char * tiempo(char * output);
int main()
{
  char output[128];
  int sock, connected, bytes_recieved , true = 1;  
  char send_data [1024] , recv_data[1024];       

  struct sockaddr_in server_addr,client_addr;    
  int sin_size;

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("Socket");
      exit(1);
  }

  if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int)) == -1) {
      perror("Setsockopt");
      exit(1);
  }

  server_addr.sin_family = AF_INET;         
  server_addr.sin_port = htons(5000);     
  server_addr.sin_addr.s_addr = INADDR_ANY; 
  bzero(&(server_addr.sin_zero),8); 

  if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))== -1) 
  {
      perror("Unable to bind");
      exit(1);
  }

  if (listen(sock, 5) == -1) {
      perror("Listen");
      exit(1);
  }

  printf("\nServidor TCP esperando clientes en el puerto 5000");
  fflush(stdout);


  while(1)
  {  
    sin_size = sizeof(struct sockaddr_in);

    connected = accept(sock, (struct sockaddr *)&client_addr,&sin_size);

    printf("\n Una coneccion de (%s , %d)\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));

    while (1)
    {
      bytes_recieved = recv(connected,recv_data,1024,0);
      recv_data[bytes_recieved] = '\0';
      //puts(recv_data);
      if(strcmp(recv_data,"dame la hora"),strlen("dame la hora"))
      {
        //puts("Generando la hora");
        tiempo(output);

        send(connected,output,128,0);
        close(connected);
        break;
      }
      else
      {
        puts("No pregunto por la hora y pues no lo atendi");
        close(connected);
      }
    }
  }       
  close(sock);
  return 0;
} 

char * tiempo(char * output)
{
   time_t tiempo = time(0);
   struct tm *tlocal = localtime(&tiempo);
   strftime(output,128,"%y-%m-%d %H:%M:%S",tlocal);
   printf("La hora es %s\n",output);
   return output;
}

