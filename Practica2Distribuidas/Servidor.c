/* 
 * File:   Servidor.c
 * Author: eos
 *
 * Created on 22 de septiembre de 2015, 02:29 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <pthread.h>
#include "elemento.h"
#include "Lista.h"
#define conexiones 15

pthread_t Servidor;
pthread_t Controlador;
//pthread_t 

void *Hilo_Servidor(void *arg);
void *Hilo_Cliente(void *arg);
void *Hilo_Control(void);
elementolista BuscaElemento(nodo *temp,int idNodo);
char * tiempo(char * output);

lista listaClientes;
int bandera=0;//Cero para indicar que no pueden enviar nada 1 Enviar Hora 2 Enviar modificacion

int main(void)
{
  
  pthread_create(&Servidor,NULL,Hilo_Servidor,NULL);
  pthread_join(Servidor,NULL);
  puts("Fin del programa");
  return 0;

}

void *Hilo_Servidor(void *arg)
{
  InicializarLista(&listaClientes);  
  int contador=0;
  elementolista *tmp=NULL;
  
  char output[128];
  
  int sock, connected, bytes_recieved , true = 1;  
  char send_data [1024] , recv_data[1024];       

  struct sockaddr_in server_addr,client_addr;    
  int sin_size;

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("Socket");
      exit(1);
  }
  
  
  if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int)) == -1) {
      perror("Setsockopt");
      exit(1);
  }

  
  
  server_addr.sin_family = AF_INET;         
  server_addr.sin_port = htons(5000);     
  server_addr.sin_addr.s_addr = INADDR_ANY; 
  bzero(&(server_addr.sin_zero),8); 

  if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))== -1) 
  {
      perror("Unable to bind");
      exit(1);
  }

  if (listen(sock, conexiones) == -1) {
      perror("Listen");
      exit(1);
  }

  printf("\nServidor TCP esperando clientes en el puerto 5000\n");
  fflush(stdout);
  pthread_create(&Controlador,NULL,Hilo_Control,NULL);
  
  
  while(1)
  {
    
    sin_size = sizeof(struct sockaddr_in);
    tmp=malloc(sizeof(elementolista));
    connected = accept(sock, (struct sockaddr *)&client_addr,&sin_size);
    tmp->idConexion=connected;
    contador++;
    tmp->idnodo=contador;
    if(connected!=-1)
    {
        if(pthread_create(&(tmp->IDHilo),NULL,Hilo_Cliente,(void *)contador)==-1)
        {
            perror("No se creo el hijo y se cierra la conecion que lo intente de NUEZ\n");
            close(connected);
        }
      printf("\n Una conexion de (%s , %d) enviada al hilo %d\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port),(int)tmp->IDHilo);
      InsertarFinal(&listaClientes,*tmp);   
    }
    else
    {
      printf("Aun no hay nadie conectado\n");
    }
    
    
    
  }
}
void *Hilo_Control(void)
{
    double tiempo=10;
    time_t tiempo1,tiempo2;
    
    tiempo1=time(0);
    while(1)
    {
        //printf("Segundo -> %d\n",(int)difftime(tiempo2,tiempo1));
        if(difftime(tiempo2,tiempo1)==tiempo)
        {
            if(EsVacia(&listaClientes)==True)
            {
                printf("No hay nadie Conectado\n");
                sleep(5);
                tiempo2=time(0);
                tiempo1=time(0);
            }
            else
            {
                printf("Bandera 1 Control\n");
                bandera=1;
                printf("Espero Respuestas\n");
                sleep(5);
                printf("Mandando Horas A trabajar\n");
                //bandera=2;
                puts("t ");
                sleep(15);
                puts(" t");
                //bandera=0;
                tiempo2=time(0);
                tiempo1=time(0);
                sleep(10);
                puts("Terminando\n Esperando Conexiones\n ");
            }
        }
        else
        {
            //printf("Durmiendo Mientras esperando conexiones\n");
        }
        tiempo2=time(0);
    }
}
void *Hilo_Cliente(void *arg)
{
    char output[128];
    char send_data [1024] , recv_data[1024];
    int bytes_recieved;
    //printf("durmiendo\n");
    sleep(1);
    //printf("despertando\nTamaño de la lista -< %d\n",TamLista(&listaClientes));
    int supcontador;
    elementolista temp;
    supcontador=(int)arg;
    temp=BuscaElemento(Primero(&listaClientes),supcontador);    
    printf("Hijo creado num Lista -> %d, id Hilo %d\n",supcontador, (int)temp.IDHilo);
    while(1)
    {
        if(bandera==1)
        {
            printf("Bandear 1\n");
            tiempo(output);
            
            send(temp.idConexion,output,128,0);
            bytes_recieved = recv(temp.idConexion,recv_data,1024,0);
            recv_data[bytes_recieved] = '\0';
            printf("Dato Recibido %s\n",recv_data);
            bandera=2;
            printf("Fin de bandera 1\n");
        }
        else if(bandera==2)
        {
            printf("Bandera 2\n");
            tiempo(output);
            send(temp.idConexion,output,128,0);
            bandera=0;
            printf("Fin de bandera 2\n");
        }
        else if(bandera==0)
        {
            //printf("Esperando No hacer nada \n");
        }
        else
        {
            printf("Error de bandera\n");
        }
    }
    

}
elementolista BuscaElemento(nodo *temp,int idNodo)
{
    if(temp!=NULL)
    {
        if(temp->objeto.idnodo==idNodo)
        {
            //printf("%d\n",temp->objeto.idnodo);
            return temp->objeto;
        }
        else
        {
            return BuscaElemento(Siguiente(&listaClientes,temp),idNodo);
        }
    }
   
}

char * tiempo(char * output)
{
   time_t tiempo = time(0);
   struct tm *tlocal = localtime(&tiempo);
   strftime(output,128,"%y-%m-%d %H:%M:%S",tlocal);
   printf("La hora es %s\n",output);
   return output;
}